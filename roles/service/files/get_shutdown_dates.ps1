#
# Powershell Script to Generate a Pipe Separated String of Dates for different Timezones
#
# Written By: Pat Maloney email: pat.maloney@bsci.com
#

Param (
	[parameter(Mandatory = $true)]
	[string]$patchStartDT ,
	[parameter(Mandatory = $true)]
	[string]$patchEndDT
)

Function main() {
	# Local Variables
	$TZs = ("EST", "CST", "PST", "IST", "CHN", "GMT")

  try {
    # Call Function
    Get-DT-For-All_TZs $patchStartDT $patchEndDT
  } catch {
    Write-Host "Run GetDatetime.ps1 Failed"
    Write-Host $_.Exception.Message
  }
}

Function Get-DT-For-All_TZs($patchStartDT, $patchEndDT) {
	# Local Variables
	$tzzone = [System.TimeZoneInfo]::FindSystemTimeZoneById("UTC")
	$dt_list = @()

  Try {
		# Create Start & Finish DateTime
		$dt_utc_downtime_start = [datetime]::parseexact($patchStartDT, 'yyyyMMddHHmm', $null)
		$dt_utc_downtime_stop  = [datetime]::parseexact($patchEndDT, 'yyyyMMddHHmm', $null)

		# Loop thru Each Timezone, & Get Start & End Datetimes
		Foreach ($TZ in $TZs) {
			# Get Timezone
			$tzzone = switch ( $TZ )
			{
				"GMT" { [System.TimeZoneInfo]::FindSystemTimeZoneById("GMT Standard Time") }
				"EST" { [System.TimeZoneInfo]::FindSystemTimeZoneById("Eastern Standard Time") }
				"CST" { [System.TimeZoneInfo]::FindSystemTimeZoneById("Central Standard Time") }
				"PST" { [System.TimeZoneInfo]::FindSystemTimeZoneById("Pacific Standard Time") }
				"IST" { [System.TimeZoneInfo]::FindSystemTimeZoneById("India Standard Time") }
				"CHN" { [System.TimeZoneInfo]::FindSystemTimeZoneById("China Standard Time") }
			}

			# Add Times to Array
			$tz_start_time = [System.TimeZoneInfo]::ConvertTimeFromUtc($dt_utc_downtime_start, $tzzone)
			$dt_list += $tz_start_time.ToString("MMM dd, yyyy hh:mm tt")
			$tz_stop_time  = [System.TimeZoneInfo]::ConvertTimeFromUtc($dt_utc_downtime_stop, $tzzone)
			$dt_list += $tz_stop_time.ToString("MMM dd, yyyy hh:mm tt")
		}

		# Print Joined String
		$str_dt = $dt_list -Join '|'
		Write-Host $str_dt
  } catch {
      Write-Host "[ERROR] Function: Get-DT-For-All_TZs Failed"
      Write-Host $_.Exception.Message
  }
}

# Call Main Function
main
